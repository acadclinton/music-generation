# coding: utf-8

"""
load activation value of after embedding

pca on lower dimension and visualization
"""

import cPickle
import numpy as np
import sklearn
from sklearn.decomposition import PCA

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig = plt.figure(1)
ax = fig.add_subplot(111, projection='3d')

def visual(plt_obj, indexNumber, nameColor = 'r'):
		
	x = plt_obj[0]
	y = plt_obj[1]
	z = []

	try:
		z = plt_obj[2]
	except:
		z = np.zeros(1195)
		
	for label, xx, yy, zz in zip(indexNumber, x, y, z):
		
		if True:
		#if(label % 12 == 4):			
			ax.scatter(xx,yy,zz,color=nameColor, marker='o') 
			ax.text(xx,yy,zz,  '%s' % (str(label)), size=10, zorder=1, color=nameColor) 
		else:
			#ax.scatter(xx,yy,zz,color='g') 
			pass

	ax.set_xlabel('X Label')
	ax.set_ylabel('Y Label')
	ax.set_zlabel('Z Label')
		

def calc_distance(data):
	
	result = {}
	count = 0
	
	maxV = 0
	minV = 9999
	
	for i in range(1195):
		for j in range(i+1,1195):
				
			#print (data[i]-data[j]) ** 2
			#value = sum((data[i]-data[j]) ** 2)
			import scipy.spatial.distance as cd
			value = cd.cosine(data[i],data[j])
			
			if maxV < value:
				maxV = value
			elif minV > value:
				minV = value
							
			result[(i,j)] = value
			count = count + 1
	
	print minV,maxV,count
	print len(result)
	
	from operator import itemgetter
	result = sorted(result.items(), key=itemgetter(1))
	
	print '	short dist	'
	for i in range(10):
		print(result[i])
	print '	long dist	'
	for i in range(10):
		print(result[-i-1])
	
	return result


if __name__ == '__main__':

	str_notes = cPickle.load(open('embedValue/str_notes.dump','r'));
	embed100 = cPickle.load(open('embedValue/embed100.train.dump','r'));
	embed50 = cPickle.load(open('embedValue/embed50.train.dump','r'));
	embed10 = cPickle.load(open('embedValue/embed10.train.dump','r'));
	counts = cPickle.load(open('embedValue/count.dump','r'));
	
	num = len(str_notes)

	note1_index = []
	note1_on = {}
	note2_index = []
	note2_on = {}
	note3_index = []
	note3_on = {}
	note48_pressed = []
	note4852_pressed = []
	note485255_pressed = []
	
	
	for i in range(1195):
		
		location = np.argwhere(str_notes[i]==1)
		count = location.shape[0]
		
		if str_notes[i][48] == 1 and str_notes[i][52] == 1 and str_notes[i][55] == 1 and count <= 5:
			note485255_pressed.append(i)
		elif str_notes[i][48] == 1 and str_notes[i][52] == 1 and count <= 4:
			note4852_pressed.append(i)
		elif str_notes[i][48] == 1 and count <= 2:
			note48_pressed.append(i)
		elif(count == 1):
			note1_index.append(i)
			note1_on[i] = location[:,0]
			if note1_on[i][0] % 12 == 0:
				print i
		elif count == 2:
			note2_index.append(i)
			note2_on[i] = location[:,0]
		elif count == 3:
			note3_index.append(i)
			note3_on[i] = location[:,0]
			
	ss = []
	dd = []
	ee = []
	count = 0
	for i in range(60):
		for j in range(20):
			ss.append(embed100[i,j])
			dd.append(embed50[i,j])
			ee.append(embed10[i,j])
			count = count + 1
			if(count == num):
				break
		if(count == num):
				break
				
	embed100 = np.array(ss).T
	embed50 = np.array(dd).T
	embed10 = np.array(ee).T
	
	#calculate distance
	#distance10 = calc_distance(embed10.T)
	#distance50 = calc_distance(embed50.T)
	#distance100 = calc_distance(embed100.T)	

	pca100 = PCA(n_components=3)
	pca100.fit(embed100)
	pca50 = PCA(n_components=3)
	pca50.fit(embed50)
	pca10 = PCA(n_components=3)
	pca10.fit(embed10)
	
	#for i in note1_index:
	#	print i, note1_on[i][:]
		
	#print note2_index
	#print note2_on
	
	#note1_index = [557,559,784,785,787,788]
	#tags = [557,559,784,785,787,788]
	
	#for i in note1_index:
	#	tags.append(note1_on[i])
	
	
	find = []
	tags = []	
	for i in note48_pressed:
		tags.append(i)
	for i in note48_pressed:
		find.append(pca10.components_.T[i])
	find = np.array(find).T
	visual(find,tags)
	
	find = []
	tags = []	
	for i in note4852_pressed:
		tags.append(i)
	for i in note4852_pressed:
		find.append(pca10.components_.T[i])
	find = np.array(find).T
	visual(find,tags,nameColor='b')
	
	find = []
	tags = []	
	for i in note485255_pressed:
		tags.append(i)
	for i in note485255_pressed:
		find.append(pca10.components_.T[i])
	find = np.array(find).T
	visual(find,tags,nameColor='y')
	
	plt.show()
	
	#visual(pca50.components_)
	#visual(pca100.components_)
