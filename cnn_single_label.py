# coding: utf-8

from midi.utils import midiread, midiwrite
import numpy as np
import os
import sys

# Parameters for MIDI sampling
r=(21,109)
dt=0.2

from keras.models import Sequential
from keras.models import Model
from keras.layers import Dense,Activation,Dropout,Merge,Flatten,RepeatVector,Reshape
from keras.layers.convolutional import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers.wrappers import TimeDistributed
from keras.layers import SimpleRNN, LSTM,GRU
from keras.layers import Embedding
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.optimizers import SGD
from datetime import datetime
import glob
from slice_fetch import data_processing

maxlen=60
def arr_to_str(arr):
    s=''
    for i in arr:
        s=s+str(int(i))
    return s

christmas_files=glob.glob('./data/Christmas/*.mid')



def load_data(train_files=glob.glob('./Nottingham/train/*.mid'),validation_files=glob.glob('./Nottingham/valid/*.mid'),test_files=glob.glob('./Nottingham/test/*.mid')  ):
    #Data loading + note labeling(to 1189)
    print len(train_files)
    print train_files[0]
    print len(train_files)
    train_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in train_files]
    valid_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in validation_files]
    test_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in test_files]
    print len(train_rolls)
    all_notes=dict()
    str_notes=dict()
    label=0
    train_labels=[]
    valid_labels=[]
    test_labels=[]
    for song in train_rolls:
        ss=[]
        for note in song:
            nn=arr_to_str(note)
            if nn not in all_notes:
                all_notes[nn]=label
                str_notes[label]=note[:]
                label += 1
            ss.append(all_notes[nn])
        train_labels.append(ss)
    for song in valid_rolls:
        ss=[]
        for note in song:
            nn=arr_to_str(note)
            if nn not in all_notes:
               all_notes[nn]=label
               str_notes[label]=note[:]
               label += 1
            ss.append(all_notes[nn])
        valid_labels.append(ss)
    for song in test_rolls:
        ss=[]
        for note in song:
            nn=arr_to_str(note)
            if nn not in all_notes:
                all_notes[nn]=label
                str_notes[label]=note[:]
                label += 1
            ss.append(all_notes[nn])
        test_labels.append(ss)

    step=1
    Dim=r[1]-r[0]

    def one_hot(n,i):
        t=np.zeros(n)
        t[i]=1
        return t


    X_train=[]
    X_train_tone=[]
    Y_train=[]
    for ii,music in enumerate(train_labels):
        for i in range(0,len(music)-maxlen,step):
            X_train.append(music[i:i+maxlen])
            X_train_tone.append(music[:maxlen])
            num=music[i+maxlen]
            Y_train.append(one_hot(label,num))
    print
    X_train=np.array(X_train[:100000])
    X_train_tone=np.array(X_train_tone[:100000])
    Y_train=np.array(Y_train[:100000])
    print 'Train X:',X_train.shape
    print 'Train Y:',Y_train.shape
    X_valid=[]
    X_valid_tone=[]
    Y_valid=[]
    for music in valid_labels:
        for i in range(0,len(music)-maxlen,step):
            X_valid.append(music[i:i+maxlen])
            X_valid_tone.append(music[:maxlen])
            num=music[i+maxlen]
        
            Y_valid.append(one_hot(label,num))
    X_valid=np.array(X_valid[:20000])
    X_valid_tone=np.array(X_valid_tone[:20000])
    Y_valid=np.array(Y_valid[:20000])
    print 'Validation X:',X_valid.shape
    print 'Validation Y:',Y_valid.shape
 
    X_test=[]
    X_test_tone=[]
    Y_test=[]
    for music in test_labels:
        for i in range(0,len(music)-maxlen,step):
            X_test.append(music[i:i+maxlen])
            X_test_tone.append(music[:maxlen])
            num=music[i+maxlen]
            Y_test.append(one_hot(label,num))
    X_test=np.array(X_test[:20000])
    X_test_tone=np.array(X_test_tone[:20000])
    Y_test=np.array(Y_test[:20000])
    print 'Test X:',X_test.shape
    print 'Test Y:',Y_test.shape
    return X_train,X_train_tone,Y_train,X_valid,X_valid_tone,Y_valid,X_test,X_test_tone,Y_test,all_notes,str_notes,label

    
X_train,X_train_tone,Y_train,X_valid,X_valid_tone,Y_valid,X_test,X_test_tone,Y_test,all_notes,str_notes,label=load_data()
#X_train,X_train_tone,Y_train,X_valid,X_valid_tone,Y_valid,X_test,X_test_tone,Y_test,all_notes,str_notes,label=load_christmas_data()
print "Data loaded."

def build_cnn_model():

    embedding_dim=128

    sequence_model=Sequential()
    sequence_model.add(Embedding(label,embedding_dim,input_length=maxlen,W_regularizer='l2'))
    sequence_model.add(Reshape((1,maxlen,embedding_dim)))

    sequence_model.add(Convolution2D(64,3,3,border_mode='same',activation='relu'))
    sequence_model.add(Convolution2D(64,3,3,border_mode='same',activation='relu'))
    sequence_model.add(MaxPooling2D(pool_size=(2,2)))
    sequence_model.add(Dropout(0.25))

    sequence_model.add(Convolution2D(128,3,3,border_mode='same',activation='relu'))
    sequence_model.add(Convolution2D(128,3,3,border_mode='same',activation='relu'))
    sequence_model.add(MaxPooling2D(pool_size=(2,2)))
    sequence_model.add(Dropout(0.25))
    sequence_model.add(Flatten())

    sequence_model.add(Dense(1024))
    sequence_model.add(Dropout(0.5))
    sequence_model.add(Dense(1024,activation='relu'))
    sequence_model.add(Dropout(0.5))
    sequence_model.add(Dense(label,activation='softmax'))
    sequence_model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
    return sequence_model 
    


#Train model
def train(model,batch=256,nb_epoch=10,path=None):
    now=datetime.now()
    if path is None:
        path='./models/%i-%i-%i_%i-%i/'%(now.year,now.month,now.day,now.hour,now.minute)
    os.system('mkdir '+path)
    model.fit(X_train,Y_train,batch_size=batch,nb_epoch=nb_epoch,validation_data=(X_valid,Y_valid),verbose=1,callbacks=[ModelCheckpoint(path+'weight.{epoch:02d}-{val_loss:.2f}.hdf5')])
    return model

#Load weight from file
def load_weight(model,path):
    model.load_weights(path)
    return model


#Generate note sequence from model. filename: seed file.
def generate(model,filename='./Nottingham/test/jigs_simple_chords_11.mid'):
 
    original_roll=midiread(filename=filename,r=r,dt=dt).piano_roll
    output_labels=[]
    for i in range(maxlen):
        nn=arr_to_str(original_roll[i])
        output_labels.append(all_notes[nn])
    gen=300
    for g in range(gen):
        
        x=np.array([output_labels[g:g+maxlen]])
        x_tone=np.array([output_labels[:maxlen]])
        result=model.predict([x],batch_size=256)
        #m=np.argmax(result)
        result=result/np.sum(result)
        m=np.random.choice(range(label),size=1,p=result[0])[0]
        output_labels.append(m)
    # In[263]:
    output_roll=[]
    
    for l in output_labels:
        output_roll.append(np.array(str_notes[l]))

    rr=filename.rfind('/')
    filename_original='/home/leesy714/Dropbox/midi/0808/%s_original.mid'%(filename[rr+1:-4])
    filename='/home/leesy714/Dropbox/midi/0808/%s_outputCNN.mid'%(filename[rr+1:-4])
    midiwrite(filename, output_roll, r=r,dt=dt)
    midiwrite(filename_original, original_roll, r=r,dt=dt)
    

# Get embedding activation of data set X
# 
def get_embedding_activation(model,X):
    model2=Sequential()
    model2.add(Embedding(label,100,input_length=maxlen,weights=model.layers[0].get_weights()))
    activation=model2.predict(X,batch_size=32)
    return activation

if __name__=='__main__':
    model=build_cnn_model()
    if len(sys.argv)>1:
        model=load_weight(model,sys.argv[1])
    else:
        model=train(model,batch=128,nb_epoch=100)
    if len(sys.argv)>2:
        filename=sys.argv[2]
        generate(model,filename=filename)
    else:
        for f in test_files:
            generate(model,filename=f)
        

