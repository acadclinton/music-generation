#!coding=utf-8

from midi.utils import midiread, midiwrite

from midi.MidiOutStream import MidiOutStream
from midi.MidiInFile import MidiInFile

import numpy as np
import scipy.stats as stats
import os
import sys
import glob
import collections


def arr_to_str(arr):
    s=''
    for i in arr:
        s=s+str(int(i))
    return s
    
def one_hot(n,i):
    t=np.zeros(n).astype(int)
    t[i]=1
    return t

class MidiInFileByTrack(MidiInFile):
    def read(self):
        "Start parsing the file"
        p = self.parser
        p.parseMThdChunk()
        for t in range(p.nTracks):  ############# each track extract can be happen
            p._current_track = t
            p.parseMTrkChunk() # this is where it's at!
        p.dispatch.eof()

    def readSep(self, midi):

        if midi.currentTrack == 0:
            self.p = self.parser
            self.p.parseMThdChunk()
            midi.nTrack = self.p.nTracks

        midi.currentTrack += 1

        if midi.currentTrack >= midi.nTrack:
            self.p.dispatch.eof()
            return False
        
        self.p._current_track = midi.currentTrack
        self.p.parseMTrkChunk() # this is where it's at!

        return True

class midireadByTrack(midiread):
    def __init__(self, filename, r=(21, 109), dt=0.2):
        self.notes = []
        self.notesTrack = []
        self._tempo = 500000
        self.beat = 0
        self.time = 0.0
        self.nTrack = 0
        self.currentTrack = 0
        
        midi_in = MidiInFileByTrack(self, filename)
        
        #midi_in.read()
        counter = 0
        while midi_in.readSep(self):
            self.notes = [n for n in self.notes if n[2] is not None]  # purge incomplete notes
            self.notesTrack.append(self.notes)
            self.notes = []

def getNotesEnergy(notes):
    if len(notes) == 0:
        return 0, 0

    note=[]
    beat=[]

    for i in range(len(notes)):
        temp = np.round(notes[i][2]-notes[i][1],2)
        if temp == 0: continue
        beat.append(temp)
        note.append(notes[i][0])

    countsNote = collections.Counter(note)
    probsNote = [float(c) / len(note) for c in countsNote.values()]
    countsBeat = collections.Counter(beat)
    probsBeat = [float(c) / len(beat) for c in countsBeat.values()]

    entropy_note = stats.entropy(probsNote)
    entropy_beat = stats.entropy(probsBeat)

    return entropy_note, entropy_beat 

def notesToData(notesInfo, r):
    piano = np.ones(r[1]-r[0]).astype(int) # all note pressed means start
        
    start = notesInfo[notesInfo[:, 1].argsort()]
    end = notesInfo[notesInfo[:, 2].argsort()]
    
    num = len(start)
        
    current = 0
    flagS = flagE = 0
    
    result_piano = []
    result_beat = []
    beat = []
    
    result_piano.append(arr_to_str(piano)) # append starting note code
    beat.append(0) # append starting beat code
    
    piano = np.zeros(r[1]-r[0]).astype(int)
    
    while not (flagS == num and flagS == flagE):
        
        while flagS != num and current == start[flagS][1]:
            piano[int(start[flagS][0])-r[0]] = 1
            flagS += 1
            
        while flagE != num and current == end[flagE][2]:
            piano[int(end[flagE][0])-r[0]] = 0
            flagE += 1
        
        result_piano.append(arr_to_str(piano))
        beat.append(current)
        
        if flagS != num and start[flagS][1] <= end[min(flagE,num-1)][2]:
            current = start[flagS][1]
        else:
            current = end[min(flagE,num-1)][2]
    
    for j,k in zip(beat,beat[1:]):
        result_beat.append(np.round(k-j,2))
        
    del result_piano[-1]
        
    piano = np.ones(r[1]-r[0]).astype(int)
    piano[r[1]-r[0]-1] = 0 # append ending note code
            
    result_piano.append(arr_to_str(piano)) # append ending note code
    result_beat.append(10000) # append ending beat code
    
    for i,j in zip(result_piano,result_beat):
        pass
        #print i,j
    
    return result_piano, result_beat

def getTrackData(filename):
    midiRead = midireadByTrack(filename = filename)

    midiRead.notes = [n for n in midiRead.notes if n[2] is not None]  # purge incomplete notes
    notesTrackInfo = np.array(midiRead.notesTrack)

    return notesTrackInfo

def everyTrack(tracks):
    notes = []

    for i in range(len(tracks)):
        notes += tracks[i]

    return notes
    
def writeMidi(notes):
    pass

def highEntropyTrack(tracks):
    maxEntropy = 0.0
    maxEntropyTrack = entroBeat = entroNote  = 0

    for i in range(len(tracks)):
        entropy = getNotesEnergy(tracks[i])

        if entropy == 0: continue
        if entropy[1] == 0: continue # beat can't be zero

        # 가정, 음은 다양하고, 박자는 안 다양하면 메인 트랙일듯
        entroNote = entropy[0]
        entroBeat = entropy[1] 
        
        if maxEntropy < entroNote / entroBeat:
            maxEntropy = entroNote / entroBeat
            maxEntropyTrack = i
    
    if maxEntropyTrack == 0: return []

    notes = np.array(tracks[maxEntropyTrack]) # 특정 트랙 반환

    return notes

class MusicDataProcess:
    def __init__(self, train, valid, test, dataShift=1, r=(21,109),slice_size=20):
    
        if dataShift < 0:
            print '$dataShift should be bigger than 0'
            
        self.labelNote = 0
        self.labelBeat = 0

        self.r = r
        self.slice_size=slice_size

        self.all_notes=dict()
        self.str_notes=dict()
        self.all_beats=dict()
        self.str_beats=dict()

        self.all_beats_count=dict()

        self.train_files = glob.glob(train)
        self.valid_files = glob.glob(valid)
        self.test_files = glob.glob(test)
        self.dataShift = dataShift
        self.beat = []
    
        self.n_song_train = len(self.train_files)
        self.n_song_valid = len(self.valid_files)
        self.n_song_test = len(self.test_files)
        
        self.labelTrain = []
        self.labelValid = []
        self.labelTest = []
        
<<<<<<< HEAD
        self.check_chords(self.train_files)
        self.check_chords(self.valid_files)
        self.check_chords(self.test_files)
        print 'N train songs:',self.n_song_train
        print 'N valid songs:',self.n_song_valid
        print 'N test songs:',self.n_song_test
=======
        
        print 'checking datas'
        self.n_train_notes=self.check_chords(self.train_files)
        self.n_valid_notes=self.check_chords(self.valid_files)
        self.n_test_notes=self.check_chords(self.test_files)
        print 'N train notes:',self.n_train_notes
        print 'N valid notes:',self.n_valid_notes
        print 'N test notes:',self.n_test_notes
>>>>>>> 054a55ccc654fabd4153c5602389181a31be6d59
        print 'N notes:',self.labelNote
        print 'N beats:',self.labelBeat
    
    def check_chords(self, files):
        total_notes=0
        for f in files:
            
            try :
                tracks = getTrackData(f)
                notes,beats = notesToData( highEntropyTrack(tracks), self.r )
                
                #writeMidi(highEntropyTrack(tracks), self.r)
                #notes,beats = notesToData( everyTrack(tracks) )
            except :
                print f, ' is not good, fail to check'
                continue
        
            for note, beat in zip(notes, beats):
                
                nn=arr_to_str(note)
                
                if nn not in self.all_notes:
                    self.all_notes[nn] = self.labelNote
                    self.str_notes[self.labelNote] = note[:]
                    self.labelNote += 1
                    
                bb=beat 
                
                if bb not in self.all_beats:
                    self.all_beats[bb] = self.labelBeat
                    self.str_beats[self.labelBeat] = bb
                    self.labelBeat += 1
            
            n_song=len(notes)
            if n_song>=self.slice_size+1:
                total_notes += (n_song-self.slice_size)/self.dataShift
        return total_notes
        

                    self.all_beats_count[bb] = 1
                else:
                    temp = self.all_beats_count[bb]
                    del(self.all_beats_count[bb])
                    self.all_beats_count[bb] = temp+1
    
    def flow(self,target='train', batch_size=128,verbose=0):
        song=0
        X_note_batch=[]
        X_beat_batch=[]
        Y_note_batch=[]
        Y_beat_batch=[] 
        if verbose>0:print 'Target:',target
        if target=='valid':
            target_files=self.valid_files
        elif target=='test':
            target_files=self.test_files
        else:
            target_files=self.train_files
        song_permutation=np.random.permutation(len(target_files))
        while True:
<<<<<<< HEAD
            
            tracks = getTrackData(target_files[song])
            piano, beats = notesToData( highEntropyTrack(tracks), self.r )
            try:
                pass
                #notes,beats = notesToData( everyTrack(tracks) )
            except:
                print target_files[song], ' is not good, fail to fetching'
                song = (song + 1)%len(target_files)
                continue
            
=======
            if target=='train':
                s=song_permutation[song]
            else:
                s=song
            piano,beats=getNotesData(target_files[s],self.r)
>>>>>>> 054a55ccc654fabd4153c5602389181a31be6d59
            n_song=len(piano)

            song = (song + 1)%len(target_files)  
            if verbose>0:print 'Song:',song,'/',len(target_files),'\t',target_files[song],n_song
            if n_song<self.slice_size+1: continue
            for slice_start in range(0,n_song - self.slice_size,self.dataShift):
                if verbose>1:print slice_start+1,'/',n_song-self.slice_size
                x_note=[]
                x_beat=[]
                for  s in range(self.slice_size):
                    x_note.append( self.all_notes[piano[slice_start + s]] )
<<<<<<< HEAD
                    #x_beat.append( one_hot(self.labelBeat,self.all_beats[beats[slice_start + s]]) )
                    x_beat.append( self.all_beats[beats[slice_start + s]] )
=======
                    x_beat.append( self.all_beats[beats[slice_start + s]] ) 
>>>>>>> 054a55ccc654fabd4153c5602389181a31be6d59
                    
                y_note=one_hot(self.labelNote, self.all_notes[piano[slice_start+ self.slice_size]])
                y_beat=one_hot(self.labelBeat, self.all_beats[beats[slice_start+ self.slice_size]])
                X_note_batch.append(x_note)
                X_beat_batch.append(x_beat)
                Y_note_batch.append(y_note)
                Y_beat_batch.append(y_beat)
                if verbose>1:print 'Batches:',len(X_note_batch)
                if len(X_note_batch)==batch_size:
                    if verbose>1:print 'Yield'
                    yield np.array(X_note_batch),np.array(X_beat_batch),np.array(Y_note_batch),np.array(Y_beat_batch)
                    X_note_batch=[]
                    X_beat_batch=[]
                    Y_note_batch=[]
                    Y_beat_batch=[]

if __name__ == "__main__":
    #Initialize with pathes of data files, shift(default 1), r (default 21,109) and slice size (default 20)
    '''
    datagen=MusicDataProcess(
        train='/home/tony/data/Nottingham/train/*.mid',
        valid='/home/tony/data/Nottingham/valid/*.mid',
        test='/home/tony/data/Nottingham/test/*.mid',
    )
    '''
    datagen=MusicDataProcess(
        #train='/home/tony/data/christmas/train/*.mid',
        train='/home/tony/data/christmas/train/rudolf2.mid',
        valid='/home/tony/data/test/valid/*.miv',
        test='/home/tony/data/test/test/*.miv',
    )
    
    # fetch a batch with flow method
    # target: 'train' 'valid' 'test'
    # verbose: True -> show debugging message

    #"""
    for x_note_batch, x_beat_batch, y_note_batch,y_beat_batch in datagen.flow(target='train',batch_size=128,verbose=1):
        print '',
    #"""

    # divide data by track....
    #print datagen.all_beats_count
