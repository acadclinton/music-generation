# coding: utf-8

from midi.utils import midiread, midiwrite
import numpy as np
import os
import sys
import glob
import collections

def arr_to_str(arr):
    s=''
    for i in arr:
        s=s+str(int(i))
    return s
    
def one_hot(n,i):
    t=np.zeros(n).astype(int)
    t[i]=1
    return t

def getNotesData(filename, r):
    #midiIn.read()

	midi_in = MidiInFile(filename)
	p = mid_in.parser
	p.parseMThdChunk() #####<<<<<

    for t in range(p.nTracks):  ############# each track extract can be happen
    	p._current_track = t
    	parseMTrkChunk(p) # this is where it's at!
    p.dispatch.eof()
    
    print midi_in.notes

    midiRead = midiread(filename = filename, r = r)
    midiRead.notes = [n for n in midiRead.notes if n[2] is not None]  # purge incomplete notes
    notesInfo = np.array(midiRead.notes).round(2)

    piano = np.ones(r[1]-r[0]).astype(int) # all note pressed means start
        
    start = notesInfo[notesInfo[:, 1].argsort()]
    end = notesInfo[notesInfo[:, 2].argsort()]
    
    num = len(start)
        
    current = 0
    flagS = flagE = 0
    
    result_piano = []
    result_beat = []
    beat = []
    
    result_piano.append(arr_to_str(piano)) # append starting note code
    beat.append(0) # append starting beat code
    
    piano = np.zeros(r[1]-r[0]).astype(int)
    
    while not (flagS == num and flagS == flagE):
        
        while flagS != num and current == start[flagS][1]:
            piano[int(start[flagS][0])-r[0]] = 1
            flagS += 1
            
        while flagE != num and current == end[flagE][2]:
            piano[int(end[flagE][0])-r[0]] = 0
            flagE += 1
        
        result_piano.append(arr_to_str(piano))
        beat.append(current)
        
        if flagS != num and start[flagS][1] <= end[min(flagE,num-1)][2]:
            current = start[flagS][1]
        else:
            current = end[min(flagE,num-1)][2]
    
    for j,k in zip(beat,beat[1:]):
        result_beat.append(k-j)
        
    del result_piano[-1]
        
    piano = np.ones(r[1]-r[0]).astype(int)
    piano[r[1]-r[0]-1] = 0 # append ending note code
            
    result_piano.append(arr_to_str(piano)) # append ending note code
    result_beat.append(10000) # append ending beat code
    
    for i,j in zip(result_piano,result_beat):
        pass
        #print i,j
    
    return result_piano, result_beat


class data_processing:
            
    def __init__(self, train, valid, test, dataShift=1, r=(21,109), dt=(0.2), sliceSize=-1):
    
        if sliceSize < 0:
            print '$sliceSize should be bigger than 0'
        if dataShift < 0:
            print '$dataShift should be bigger than 0'
            
        self.labelNote = 0
        self.labelBeat = 0

        self.r = r
        self.dt = dt

        self.all_notes=dict()
        self.str_notes=dict()
        self.all_beats=dict()
        self.str_beats=dict()

        self.train_files = glob.glob(train)
        self.valid_files = glob.glob(valid)
        self.test_files = glob.glob(test)
        self.dataShift = dataShift
        self.sliceSize = sliceSize
        self.beat = []
    
        self.songTrain = self.SongTrain = self.noteTrain = self.NoteTrain = 0
        self.songValid = self.SongValid = self.noteValid = self.NoteValid = 0
        self.songTest = self.SongTest = self.noteTest = self.NoteTest = 0
        
        self.SongTrain = len(self.train_files)
        self.SongValid = len(self.valid_files)
        self.SongTest = len(self.test_files)
        
        self.labelTrain = []
        self.labelValid = []
        self.labelTest = []
        
        
        print 'checking datas'
        self.numberOfBatchTrain = self.check_chords(self.train_files,r,dt,sliceSize,dataShift)
        self.numberOfBatchValid = self.check_chords(self.valid_files,r,dt,sliceSize,dataShift)
        self.numberOfBatchTest = self.check_chords(self.test_files,r,dt,sliceSize,dataShift)

        print 'batch Size : ', self.numberOfBatchTrain, self.numberOfBatchValid, self.numberOfBatchTest , 'chord : ', self.labelNote, 'beat_Var : ', self.labelBeat
    
    def check_chords(self, files, r, dt, sliceSize, dataShift):
        batchCount = 0
        
        for f in files:
            try :
                notes, beats = getNotesData(f, r)
            except :
                print f, ' is not good, fail to check'
                continue
        
            for note, beat in zip(notes, beats):
                
                nn=arr_to_str(note)
                
                if nn not in self.all_notes:
                    self.all_notes[nn] = self.labelNote
                    self.str_notes[self.labelNote] = note[:]
                    self.labelNote += 1
                    
                bb=beat
                
                if bb not in self.all_beats:
                    self.all_beats[bb] = self.labelBeat
                    self.str_beats[self.labelBeat] = bb
                    self.labelBeat += 1

            batchCount += int((len(notes) - sliceSize) / dataShift)

        return batchCount
        
    def packMusic(self, filename):
        ss=[] # note sequence for a song
        sss=[] # beat sequence for a song
        
        # 기존에는 음원을 샘플링 하고 그에 대한 압축으로 박자를 얻었음
        """ 
        for note in song:
            
            nn=self.all_notes[arr_to_str(note)]
            
            if first_start == 1:
                first_start = 0
                sameCount = 1
                before_nn = nn
                continue
                
            if before_nn == nn:
                sameCount += 1
            else:
                ss.append(before_nn)
                sss.append(np.min([self.beat[sameCount][0], self.maxBeatConsider-1]))
                #sss.append(one_hot(self.maxBeatConsider,
                #                   np.min([self.beat[sameCount][0],
                #                           self.maxBeatConsider-1])).tolist())
                sameCount = 1
                
            before_nn = nn # remember previous note
        
        ss.append(nn) #음
        sss.append(np.min([self.beat[sameCount][0], self.maxBeatConsider-1]))
        #sss.append(one_hot(self.maxBeatConsider,
        #                   np.min([self.beat[sameCount][0],
        #                          self.maxBeatConsider-1])).tolist())#박
        
        return [ss,sss]
        """
        # 이번에는 음원의 박자 정보를 기초하여 정보를 얻어 올 것.
        
        notes, beats = getNotesData(filename, r)
        
        for note, beat in zip(notes, beats):
            
            nn=self.all_notes[arr_to_str(note)]
            bb=self.all_beats[beat]
            
            ss.append(nn)
            sss.append(bb)
            
        return [ss,sss]
        
    def __slice(self, song, Song, note, Note, label, files, sliceSize, dataShift):
        result = [[],[],[],[]]
        isEmpty = False
        
        noteFlag = note
        
        while song < Song:
            MUSIC = []            
            if label == []:
                try :
                    label = getNotesData(files[song], self.r)
                except:
                    print 'fail to analysis ', files[song]
                    song += 1
                    continue
                
                noteFlag = 0
                Note = len(label[0])
                
                if sliceSize + 1 >= Note-1:
                    print 'song is shorter than sliceSize : ', files[song]
                    #print Note, noteFlag, sliceSize
                    song += 1
                    label = []
                    continue
                
            while True:
                result[0].append(self.all_notes[label[0][noteFlag]])
                result[1].append(self.all_beats[label[1][noteFlag]])
                
                noteFlag += 1
                sliceSize -= 1
                
                if sliceSize == 0:
                    break
                                
            result[2].append(one_hot(self.labelNote, self.all_notes[label[0][noteFlag]]))
            result[3].append(one_hot(self.labelBeat, self.all_beats[label[1][noteFlag]]))
            
            #print result[0], result[2], result[1], result[3]
            #result[3].append(label[1][noteFlag])
                    
            if noteFlag == Note-1:
                song += 1
                label = []
                note = 0
            
            if sliceSize == 0:
                break
                
        note += dataShift
        
        isEmpty = False
        
        if song == Song:
            song = 0
            isEmpty = True
        
        return [result, isEmpty, song, Song, note, Note, label]
                    
    def slice_fetch(self, obj='', dataShift=1, batchSize=-1):
        returnResult = [[],[],[],[]]
        isEmpty = False
        
        if batchSize == -1:
            print '$batchSize should be bigger than 0(0 means 100,000,000)'
        elif batchSize == 0:
            batchSize = 100000000

        batchIter = 0

        for batchIter in range(batchSize):
            
            if obj == 'train':
                [result, isEmpty, self.songTrain, self.SongTrain, self.noteTrain, self.NoteTrain, self.labelTrain] = \
                     self.__slice(self.songTrain, self.SongTrain, self.noteTrain, self.NoteTrain, self.labelTrain, self.train_files, self.sliceSize, dataShift)
            elif obj == 'valid':
                [result, isEmpty, self.songValid, self.SongValid, self.noteValid, self.NoteValid, self.labelValid] = \
                     self.__slice(self.songValid, self.SongValid, self.noteValid, self.NoteValid, self.labelValid, self.valid_files, self.sliceSize, dataShift)
            elif obj == 'test':
                [result, isEmpty, self.songTest, self.SongTest, self.noteTest, self.NoteTest, self.labelTest] = \
                     self.__slice(self.songTest, self.SongTest, self.noteTest, self.NoteTest, self.labelTest, self.test_files, self.sliceSize, dataShift)
            else:
                print "&obj needs value(Among 'train', 'valid', 'test'"
                return False;
                
            for iter in range(4):
                returnResult[iter].append(result[iter])
                
            if isEmpty:
                break
		
        batchIter += 1
        
        returnResult[0] = np.array(returnResult[0])
        returnResult[1] = np.array(returnResult[1])
        returnResult[2] = np.reshape(np.array(returnResult[2]),(batchIter,self.labelNote))
        returnResult[3] = np.reshape(np.array(returnResult[3]),(batchIter,self.labelBeat))
                
        return [returnResult[0], returnResult[1], returnResult[2], returnResult[3], isEmpty]
            
if __name__ == "__main__":
    
    #"""
    header_dir = '/home/tony/data/christmas'
    #header_dir = '/home/tony/data/test'
    
    #slicer = data_processing(train = '/home/tony/data/christmas/train/rudolph.mid',#header_dir+'/train/*.mid',
    slicer = data_processing(train = header_dir+'/train/*.mid',
                             valid = header_dir+'/valid/*.mid',
                              test = header_dir+'/test/*.mid',
                             dataShift=1, sliceSize=5, r=(21,109))
           
    #usage
    
    [x_note, x_beat, y_note, y_beat, isEmpty] = slicer.slice_fetch(obj='train', batchSize=5)
    
    #"""
    """
    data_processing class 를 생성할 때
    train set, valid set, test set 의 경로를 삽입해 줍니다.
    maxBeatConsider 은 같은 음이 최대로 반복되는 수입니다.
    maxBeatConsider 보다 많은 횟수 반복된다면, 해당 숫자로 고정됩니다.
    dataShift 는 raw 데이터에서 학습용 데이터를 생성할 때, 쉬프트 하는 정도입니다.
    sliceSize 는 raw 데이터에서 학습용 데이터를 생성할 때, 생성하는 길이를 정합니다.
    
    slice_fetch()는 데이터를 fetch하는 함수입니다.
    batchSize 만큼의 학습용 데이터 셋을 가져옵니다.
    
    결과로는 x_note, x_beat, y_note, y_beat, isEmpty 가 반환됩니다.
    isEmpty는 해당 폴더를 모두 읽으면 True 값을 가지게 됩니다. 그 경우 데이터를 처음부터 다시 가져옵니다.
    
    사용 시 얻는 결과에 대한 예:
    
    음원 note = [1 2 3 4 5 6]
    음원 beat = [3 1 2 3 3 2] <- one_hot 대신 수로 표현
    이 주어졌을때

    sliceSize = 3, batchSize = 2
    x_note : [[1 2 3], [2 3 4]]
    x_beat : [[3 1 2], [1 2 3]]
    y_note : [[4], [5]]
    y_beat : [[3], [3]]

    sliceSize = 3, batchSize = 1
    x_note : [[1 2 3]]
    x_beat : [[3 1 2]]
    y_note : [[4]]
    y_beat : [[3]]

    sliceSize = 4, batchSize = 2
    x_note : [[1 2 3 4], [2 3 4 5]]
    x_beat : [[3 1 2 3], [1 2 3 3]]
    y_note : [[5], [6]]
    y_beat : [[3], [2]]

    sliceSize = 4, batchSize = 1
    x_note : [[1 2 3 4]]
    x_beat : [[3 1 2 3]]
    y_note : [[5]]
    y_beat : [[3]]

    """

