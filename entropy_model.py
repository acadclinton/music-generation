#coding: utf-8

#THEANO usage
#THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 python entropy_model.py
#THEANO_FLAGS=mode=FAST_RUN,device=gpu,floatX=float32 ju$yter notebook

# import sep_model

from midi.utils import midiwrite, midiread
from midi.MidiOutFile import MidiOutFile
import numpy as np
import os
import sys

from MusicDataProcess import MusicDataProcess as MDP

from keras.models import Sequential
from keras.layers import Dense,Activation,Dropout
from keras.layers import SimpleRNN, LSTM
from keras.layers import Embedding
from keras.layers import Merge, Flatten
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.optimizers import SGD
from datetime import datetime
import glob

# Parameters for MIDI sampling
r=(21,109)
step=1

embed_size=100 # 임베딩레이어의 크기

maxRefLength=20 # 참고 할 시퀀스 수

# 전역변수 area
labelNote = 0
labelBeat = 0

dataSetUse = ''

#Build model
def build_note_model():
	#print 'Building model...'
	
	note_model=Sequential()
	note_model.add(Embedding(labelNote,embed_size,input_length=maxRefLength))
	
	beat_model=Sequential()
	beat_model.add(Embedding(labelBeat,embed_size,input_length=maxRefLength))
	
	model = Sequential()
	model.add(Merge([note_model, beat_model], mode='concat'))
	model.add(Flatten())
	
	model.add(Dense(200,activation='relu'))
	model.add(Dense(labelNote,init='he_normal',activation='softmax'))
	
	model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
	return model

def build_beat_model():
	#print 'Building model...'
	
	note_model=Sequential()
	note_model.add(Embedding(labelNote,embed_size,input_length=maxRefLength))
	
	beat_model=Sequential()
	beat_model.add(Embedding(labelBeat,embed_size,input_length=maxRefLength))
	
	model = Sequential()
	model.add(Merge([note_model, beat_model], mode='concat'))
	model.add(Flatten())
	
	model.add(Dense(200,activation='relu'))
	model.add(Dense(labelBeat,init='he_normal',activation='softmax'))
	
	model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
	return model


#Train model
def train(X,Y,model,X_valid=None,Y_valid=None,batch=256,nb_epoch=50,path=None,notebeat=''):
	now=datetime.now()

	if path is None:
		path= '/home/tony/exp/models/' + dataSetUse + '[' + notebeat + ']' + '%i-%i-%i_%i-%i_max-%d_emb-%d_step-%d_label-%d' % (now.year,now.month,now.day,now.hour,now.minute,maxRefLength,embed_size,step,labelNote)
	
	""" usage 
	model.fit(X_train,Y_train,batch_size=batch,
			   nb_epoch=nb_epoch, validation_data=(X_valid,Y_valid),
			   verbose=1, callbacks=[ModelCheckpoint(path+'weight.{epoch:02d}-{val_loss:.2f}.hdf5')])
	"""
	model.fit(X,Y,batch_size=batch, # validation_data=(X_valid,Y_valid),
			  nb_epoch=nb_epoch,verbose=1,
			  callbacks=[ModelCheckpoint(path+'_weight.hdf5')])

	return model

#Load weight from file
def load_weight(model,path):
	model.load_weights(path)
	return model


#Generate note sequence from model. filename: seed file.
def generate(note_model,beat_model,all_notes,str_notes,all_beats,str_beats,
			 filename='/home/tony/data/christmas/train/rudolph.mid', iternum=0):
	
	output_note=[]
	output_beat=[]
	
	original_notes, original_beats = sf.getNotesData(filename,r)
	
	if len(original_notes) < maxRefLength:
		print 'basis music is too short than $maxRefLength'
		return
	
	for i in range(maxRefLength):
		nn=all_notes[arr_to_str(original_notes[i])]
		bb=all_beats[original_beats[i]]
		
		output_note.append(nn)
		output_beat.append(bb) # 박자는 모두 가장 많이 나온거로
	
	start_point = 0
	run_time = 0

	while True:
	
		x_note = np.array([output_note[start_point:start_point+maxRefLength]])#음
		x_beat = np.array([output_beat[start_point:start_point+maxRefLength]])#박
		
		result_note = note_model.predict([x_note, x_beat],batch_size=8)
		result_beat = beat_model.predict([x_note, x_beat],batch_size=8)

		m_note=np.random.choice(range(labelNote),size=1,p=result_note[0])[0]
		m_beat=np.random.choice(range(labelBeat),size=1,p=result_beat[0])[0]
		
		countOneSeq = 0
		for i in str_notes[m_note]:
			if i == '1':
				countOneSeq += 1
			else:
				break
		
		if str_beats[m_beat] == 10000 and countOneSeq == r[1]-r[0]-1:
			output_note.append(m_note)
			output_beat.append(m_beat)
			break
		elif str_beats[m_beat] == 10000:
			print start_point, run_time, 'beat turn'
			break
			continue
		elif countOneSeq == r[1]-r[0]-1:
			print start_point, run_time, 'note turn'
			break
			continue

		output_note.append(m_note)
		output_beat.append(m_beat)

		start_point += 1
		run_time += str_beats[m_beat]
		#print str_notes[m_note], str_beats[m_beat], start_point
		
	# convert labeled infomation to real Info
	del output_note[0]
	del output_beat[0]
	
	notes = np.zeros((len(output_note),r[1]-r[0])).astype(int)
	beats = []
	
	seq = 0
	for i,j in zip(output_note, output_beat):
		
		index = 0
		for ii in str_notes[i]:
			notes[seq][index] = ii
			index += 1
		seq += 1
		beats.append(str_beats[j])
				
	now = datetime.now()
	rr=filename.rfind('/')
	filename='/home/tony/exp/result/' + dataSetUse  + '/%s[%i-%i_%i-%i-%i.%d].mid'%(filename[rr+1:-4],now.month,now.day,now.hour,now.minute,now.second,iternum)
	
	midi = MidiOutFile(filename)
	midi.header(division=100)
	midi.start_of_track() 
	midi.patch_change(channel=0, patch=0)
	t = 0  

	#print piano_roll.nonzero()
		
	samples = [i.nonzero()[0] + r[0] for i in notes]
	
	for i in xrange(len(samples)):

		for f in samples[i]:
			if i==0 or f not in samples[i-1]:
				midi.update_time(t)
				midi.note_on(channel=0, note=f, velocity=90)
				t = 0

		#print beats[i],len(samples),i,len(beats)
		t += int(beats[i]*200)

		for f in samples[i]:
			if i==len(samples)-1 or f not in samples[i+1]:
				midi.update_time(t)
				midi.note_off(channel=0, note=f, velocity=0)
				t = 0
		  
	midi.update_time(0)
	midi.end_of_track()
	midi.eof()

# Get embedding activation of data set X

def get_embedding_activation(model,X):
	model2=Sequential()
	model2.add(Embedding(labelNote,embed_size,input_length=maxRefLength,weights=model.layers[0].get_weights()))
	activation=model2.predict(X,batch_size=32)
	return activation

def arr_to_str(arr):
	s=''
	for i in arr:
		s=s+str(int(i))
	return s    
	
def one_hot(n,i):
	t=np.zeros(n).astype(int)
	t[i]=1
	return t
	
if __name__=='__main__':
	
	dataSetUse = 'test' # for saving data
	#dataSetUse = 'Nottingham' # for saving data
	os.system('mkdir /home/tony/exp/models/' + dataSetUse)
	os.system('mkdir /home/tony/exp/result/' + dataSetUse)

	header_dir = '/home/tony/data/'+dataSetUse

	datagen = MDP(train = header_dir+'/train/*.mid',
										 valid = header_dir+'/valid/*.mid',
										 test = header_dir+'/test/*.mid')
	labelNote = datagen.labelNote
	labelBeat = datagen.labelBeat
		
	note_model=build_note_model()
	beat_model=build_beat_model()
	
	#"""    these for learning
	
	for x_note_batch, x_beat_batch, y_note_batch,y_beat_batch in datagen.flow(target='train',batch_size=3,verbose=1):
		
		print x_note_batch.shape, x_beat_batch.shape, y_note_batch.shape
		print x_note_batch, x_beat_batch, y_note_batch
		note_model = train(X=[x_note_batch, x_beat_batch], Y=y_note_batch,
						   model=note_model, batch=128, nb_epoch=30, notebeat='note')
		beat_model = train(X=[x_note_batch, x_beat_batch], Y=y_beat_batch,
						   model=beat_model, batch=128, nb_epoch=30, notebeat='beat')
		
		
			
			
	print 'learning done'
	#"""
	
	""" these for generating
	#note_model=load_weight(note_model,'/home/tony/exp/models/note')
	#beat_model=load_weight(beat_model,'/home/tony/exp/models/beat')
	note_model=load_weight(note_model,'/home/tony/exp/models/Nottingham_note_20.hdf5')
	beat_model=load_weight(beat_model,'/home/tony/exp/models/Nottingham_beat_20.hdf5')
	for i in range(10):
		generate(note_model, beat_model, slicer.all_notes, slicer.str_notes,
									     slicer.all_beats, slicer.str_beats, iternum=i)
				#filename='/home/tony/data/Nottingham/test/reels_simple_chords_215.mid')
	#"""
	

