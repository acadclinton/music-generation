from MidiOutStream import MidiOutStream
from MidiInFile import MidiInFile

"""
This prints all note on events on midi channel 0
"""


class Transposer(MidiOutStream):
    
    "Transposes all notes by 1 octave"
    
    def note_on(self, channel=0, note=0x40, velocity=0x40):
        if channel == 0:
            print channel, note, velocity, self.rel_time()


event_handler = Transposer()

#<<<<<<< HEAD
in_file = 'jigs_simple_chords_121.mid'
#=======
in_file = 'midiout/minimal_type0.mid'
#>>>>>>> f660e5b56c3e648f3947e5e0e61817c81be89453
midi_in = MidiInFile(event_handler, in_file)
midi_in.read()

