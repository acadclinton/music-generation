
from midi.utils import midiread, midiwrite
import numpy as np
import os
import sys

import glob
# Parameters for MIDI sampling
r=(21,109)
dt=0.25
	
# Parameters for MIDI training
maxlen=20
step=1
Dim=r[1]-r[0]
embed_size=10

def arr_to_str(arr):
	s=''
	for i in arr:
		s=s+str(int(i))
	return s
	
def one_hot(n,i):
	t=np.zeros(n)
	t[i]=1
	return t

																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																													
if __name__=='__main__':

	#Data loading + note labeling(to 1189)
	train_file_address = './Nottingham/train/*.mid'
	train_files=glob.glob(train_file_address)
	validation_files=glob.glob('./Nottingham/valid/*.mid')
	test_files=glob.glob('./Nottingham/test/*.mid')

	print train_files[0]
	train_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in train_files]
	valid_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in validation_files]
	test_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in test_files]

	all_notes=dict()
	str_notes=dict()
	label=0
	train_labels=[]
	valid_labels=[]
	test_labels=[]
	counter=np.zeros(2000).astype(int)
	
	for song in train_rolls:
		ss=[]
		for note in song:
			nn=arr_to_str(note)
			if nn not in all_notes:
				all_notes[nn]=label
				str_notes[label]=note[:]
				label += 1
			ss.append(all_notes[nn])
		train_labels.append(ss)
		
	for song in valid_rolls:
		ss=[]
		for note in song:
			nn=arr_to_str(note)
			if nn not in all_notes:
			   all_notes[nn]=label
			   str_notes[label]=note[:]
			   label += 1
			ss.append(all_notes[nn])
		valid_labels.append(ss)
		
	for song in test_rolls:
		ss=[]
		for note in song:
			nn=arr_to_str(note)
			if nn not in all_notes:
				all_notes[nn]=label
				str_notes[label]=note[:]
				label += 1
			ss.append(all_notes[nn])
		test_labels.append(ss)


	X_train=[]
	Y_train=[]
	for ii,music in enumerate(train_labels):
		for i in range(0,len(music)-maxlen,step):
			X_train.append(music[i:i+maxlen])
			num=music[i+maxlen]
			Y_train.append(one_hot(label,num))
	print
	X_train=np.array(X_train[:100000])
	Y_train=np.array(Y_train[:100000])
	print 'Train X:',X_train.shape
	print 'Train Y:',Y_train.shape

	X_valid=[]
	Y_valid=[]
	for music in valid_labels:
		for i in range(0,len(music)-maxlen,step):
			X_valid.append(music[i:i+maxlen])
			num=music[i+maxlen]
			
			Y_valid.append(one_hot(label,num))
	X_valid=np.array(X_valid[:30000])
	Y_valid=np.array(Y_valid[:30000])
	print 'Validation X:',X_valid.shape
	print 'Validation Y:',Y_valid.shape

	X_test=[]
	Y_test=[]
	for music in test_labels:
		for i in range(0,len(music)-maxlen,step):
			X_test.append(music[i:i+maxlen])
			num=music[i+maxlen]
			Y_test.append(one_hot(label,num))
	X_test=np.array(X_test[:30000])
	Y_test=np.array(Y_test[:30000])
	print 'Test X:',X_test.shape
	print 'Test Y:',Y_test.shape
	
	print X_train
	
	for i in X_train:
		for j in i:
			counter[j] += 1;
	for i in X_valid:
		for j in i:
			counter[j] += 1;
	for i in X_test:
		for j in i:
			counter[j] += 1;
	
	print counter
	
	import cPickle
	cPickle.dump(counter,open('./embedValue/count.dump','w'))
	
