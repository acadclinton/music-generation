# coding: utf-8

#THEANO_FLAGS=mode=FAST_RUN,device=g	pu,floatX=float32 python embedding_layer.py

from midi.utils import midiread, midiwrite
import numpy as np
import os
import sys

from keras.models import Sequential
from keras.layers import Dense,Activation,Dropout
from keras.layers import SimpleRNN, LSTM
from keras.layers import Embedding
from keras.callbacks import ModelCheckpoint
from keras.regularizers import l2
from keras.optimizers import SGD
from datetime import datetime
import glob

# Parameters for MIDI sampling
r=(21,109)
dt=0.5
	
# Parameters for MIDI training
maxlen=20
step=1
Dim=r[1]-r[0]
embed_size=50

# init variable for 전역변수
X_train=[]
Y_train=[]		
X_valid=[]
Y_valid=[]

total_label = 0

#Build model
def build_model():
	print 'Building model...'
	model=Sequential()
	model.add(Embedding(label,embed_size,input_length=maxlen))
	model.add(LSTM(200))
	model.add(Dense(1000,init='he_normal',activation='relu'))
	model.add(Dense(label,init='he_normal'))
	model.add(Activation('softmax'))
	model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])
	return model
#Build model

#Train model
def train(model,batch=256,nb_epoch=50,path=None):
    now=datetime.now()
    
    if path is None:
        path='./models/%i-%i-%i_%i-%i_max-%d_embed-%d_step-%d_dt-%.2f_label-%d'%(now.year,now.month,now.day,now.hour,now.minute,maxlen,embed_size,step,dt,total_label)
        
	os.system('mkdir '+path)
    #model.fit(X_train,Y_train,batch_size=batch,nb_epoch=nb_epoch,validation_data=(X_valid,Y_valid),verbose=1,callbacks=[ModelCheckpoint(path+'weight.{epoch:02d}-{val_loss:.2f}.hdf5')])
    model.fit(X_train,Y_train,batch_size=batch,nb_epoch=nb_epoch,verbose=1,callbacks=[ModelCheckpoint(path+'weight.{epoch:02d}-{val_loss:.2f}.hdf5')])
    print 'saved done'
    
    return model

#Load weight from file
def load_weight(model,path):
    model.load_weights(path)
    return model


#Generate note sequence from model. filename: seed file.
def generate(model,filename='./Nottingham/test/jigs_simple_chords_11.mid'):																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																															
    original_roll=midiread(filename=filename,r=r,dt=dt).piano_roll
    output_labels=[]
    for i in range(maxlen):
        nn=arr_to_str(original_roll[i])
        output_labels.append(all_notes[nn])
    print output_labels 
    
    gen=300
    for g in range(gen):
        x=np.array([output_labels[g:g+maxlen]])
        result=model.predict(x,batch_size=256)
        #m=np.argmax(result)
        result=result/np.sum(result)
        m=np.random.choice(range(label),size=1,p=result[0])[0]
        output_labels.append(m)
    # In[263]:
    output_roll=[]
    
    for l in output_labels:
        output_roll.append(np.array(str_notes[l]))

    
    rr=filename.rfind('/')
    filename_original='/home/tony/219/coding/LSTM/music-generation/result/original_%s.mid'%(filename[rr+1:-4])
    filename='/home/tony/219/coding/LSTM/music-generation/result/outputLSTM_%s.mid'%(filename[rr+1:-4])
    midiwrite(filename, output_roll, r=r,dt=dt)
    midiwrite(filename_original, original_roll, r=r,dt=dt)
    

# Get embedding activation of data set X

def get_embedding_activation(model,X):
    model2=Sequential()
    model2.add(Embedding(label,embed_size,input_length=maxlen,weights=model.layers[0].get_weights()))
    activation=model2.predict(X,batch_size=32)
    return activation

def arr_to_str(arr):
	s=''
	for i in arr:
		s=s+str(int(i))
	return s
	
	
def one_hot(n,i):
	t=np.zeros(n)
	t[i]=1
	return t

if __name__=='__main__':
	
	# load valid set & test set WITH LABELING & ADDING CHORD SPACE
	
	#validation_files=glob.glob('./Nottingham/valid/*.mid')
	#valid_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in validation_files]
	#test_files=glob.glob('./Nottingham/test/*.mid')
	#test_rolls=[midiread(filename=f, r=r,dt=dt).piano_roll for f in test_files]
	
	train_file_address = './data/1/0/*.mid'
	train_files=glob.glob(train_file_address)

	all_notes=dict()
	str_notes=dict()
	
	label=0
	train_labels=[]
	valid_labels=[]
	test_labels=[]
	
	for f in train_files:
		try: # read one file as batch and learn it
			train_rolls = []
			train_rolls = [midiread(filename=f, r=r,dt=dt).piano_roll]
		
			print 'analysis ' + f;
		
			for song in train_rolls:
				ss=[]
				for note in song:
					nn=arr_to_str(note)
					if nn not in all_notes:
						all_notes[nn]=label
						str_notes[label]=note[:]
						label += 1
					ss.append(all_notes[nn])
				train_labels.append(ss)
		except:
			print 'fail to analysis ' + f;
	"""	
	for song in valid_rolls:
		ss=[]
		for note in song:
			nn=arr_to_str(note)
			if nn not in all_notes:
			   all_notes[nn]=label
			   str_notes[label]=note[:]
			   label += 1
			ss.append(all_notes[nn])
		valid_labels.append(ss)
		
	for song in test_rolls:
		ss=[]
		for note in song:
			nn=arr_to_str(note)
			if nn not in all_notes:
				all_notes[nn]=label
				str_notes[label]=note[:]
				label += 1
			ss.append(all_notes[nn])
		test_labels.append(ss)
		
	X_valid=[]
	Y_valid=[]
	for music in valid_labels:
		for i in range(0,len(music)-maxlen,step):
			X_valid.append(music[i:i+maxlen])
			num=music[i+maxlen]
			Y_valid.append(one_hot(label,num))
	
	print 'Validation X:',len(X_valid)																																																																																																	
	X_valid=np.array(X_valid[:30000])
	Y_valid=np.array(Y_valid[:30000])
	print 'Validation X:',X_valid.shape
	print 'Validation Y:',Y_valid.shape

	X_test=[]
	Y_test=[]
	for music in test_labels:
		for i in range(0,len(music)-maxlen,step):
			X_test.append(music[i:i+maxlen])
			num=music[i+maxlen]
			Y_test.append(one_hot(label,num))
			
	print 'Validation X:',len(X_test)
	X_test=np.array(X_test[:30000])
	Y_test=np.array(Y_test[:30000])
	print 'Test X:',X_test.shape
	print 'Test Y:',Y_test.shape
	"""
		
	# load train set file by file, learn the data one by one
	# Data loading & also adding chord space
	
	# load model
	# generally, model should not exist.....
	# given model might not proper
	
	print 'total number of chord : ' + str(label)
	total_label = label
	
	output_roll=[]
	model=build_model()
	
	"""
	if len(sys.argv)>1:
		model=load_weight(model,sys.argv[1])
	else:
		model=train(model,batch=128,nb_epoch=30)
	"""
	
	for f in train_files:
		try: # read one file as batch and learn it
			train_rolls = []
			train_rolls = [midiread(filename=f, r=r,dt=dt).piano_roll]
			
			print 'learning ' + f;
			
			for song in train_rolls:
				ss=[]
				for note in song:
					nn=arr_to_str(note)
					ss.append(all_notes[nn])
				train_labels.append(ss)
			
			X_train=[]
			Y_train=[]
			for ii,music in enumerate(train_labels):
				for i in range(0,len(music)-maxlen,step):
					X_train.append(music[i:i+maxlen])
					num=music[i+maxlen]
					Y_train.append(one_hot(label,num))

			X_train=np.array(X_train[:100000])
			Y_train=np.array(Y_train[:100000])
			print 'Train X:',X_train.shape
			print 'Train Y:',Y_train.shape
			
			model=train(model,batch=128,nb_epoch=1)
			
			print

		except:
			print 'already failed... ' + f
			#exit()

	if len(sys.argv)>2:
		filename=sys.argv[2]
		generate(model,filename=filename)
	else:
		generate(model)
        
	
	X_=[]
	cnt=0
	for i in range(0,label,20):
		ss=[]
		for j in range(0,20):
			ss.append(cnt)
			if cnt<label-1:
				cnt += 1
		X_.append(ss)
	X_=np.array(X_)
	print X_ 
	print X_test.shape
			
	act=get_embedding_activation(model,X_)
		
	import cPickle
	
	cPickle.dump(act,open('./embedValue/act_train.dump','w'))
	cPickle.dump(str_notes,open('./embedValue/str_notes.dump','w'))
	""" """
